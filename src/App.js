import React from 'react';
import './App.css';
import axios from 'axios';
const fs = require('fs');
const headers = { 'x-apikey': 'f26d7b2e3097d8f11ef2e895613513740675a1437570537ee5ea7662fc47376d' }

function App() {
  return (
    <div className="App">
      <input type="file" id="btnSubirArchivo" onChange={(e) => subirArchivo(e)} /> {/* Add multiple to select more than one */}
    </div>
  );
}

function subirArchivo(e) {
  console.log('evento: ', e.target.files[0])
  console.log('process env tiene: ', process.env)

  const formData = new FormData();
  formData.append('file', e.target.files[0])
  axios.post('https://www.virustotal.com/api/v3/files', formData, { headers }).then(async (res) => {
    console.log('res: ', res.data.data.id)
    let fileId = res.data.data.id;

    axios.get(`https://www.virustotal.com/api/v3/analyses/${fileId}`, { headers }).then((response) => {
      console.log('respuesta final tiene: ', response);
    }).catch(err => {
      console.log(err);
    });
  }).catch(err => {
    console.log(err);
  });

}

export default App;
